#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
#include "grader.h"

using namespace std;

inline bool CompareStudent(const pair<string, double>& a,
                           const pair<string, double>& b){
	return a.second>b.second;
}

double GetNumberGrade(const string& str) {
  if (str == "A+") return 4.5;
  if (str == "A" || str == "P") return 4.0;
  if (str == "B+") return 3.5;
  if (str == "B") return 3.0;
  if (str == "C+") return 2.5;
  if (str == "C") return 2.0;
  if (str == "D+") return 1.5;
  if (str == "D") return 1.0;
  return 0.0;
}

int main()
{

  	double total_credit = 0,total_score;
	string cmd;
	vector<pair<string, double> > student_grades;
	vector<Subject*> A;
	while (cmd != "quit")
	{
		cin >> cmd;
		if (cmd == "subject")
		{
			string subject,type;
			int credit,a,b,c,d;
			cin >> subject>>credit>>type;
			total_credit = total_credit + credit;
			if (type=="G+")
			{
				cin>>a>>b>>c>>d;
				SubjectGradePlus* fuck = new SubjectGradePlus(subject,credit,a,b,c,d);
				A.push_back(fuck);
			}
			else if (type=="G")
			{
				cin>>a>>b>>c>>d;
				SubjectGrade* fuck1 = new SubjectGrade(subject,credit,a,b,c,d);
				A.push_back(fuck1);
			}
			else if (type=="PF")
			{
				cin>>a;
				SubjectPassFail* fuck2 = new SubjectPassFail (subject,credit,a);
				A.push_back(fuck2);		
			}
		}
		if (cmd == "eval")
		{
		      	string name,grade;
			vector<string> grades;
			int score;
		      	cin >> name;
			total_score=0;
		      	for (int i=0;i<A.size();i++)
		      	{
				cin>>score;
				grade=A[i]->GetGrade(score);
				grades.push_back(grade);
				total_score+=( GetNumberGrade(grade) * ( A[i]->credit() ) );
			}
			cout << name << ' ';
			for(int i=0;i<grades.size();i++) cout << grades[i]<<' ';
			student_grades.push_back(make_pair(name,(total_score)/(total_credit)));
			cout<<endl;
		}
	}
	sort(student_grades.begin(), student_grades.end(), CompareStudent);
	for (int i = 0; i < student_grades.size(); ++i)
	{
		cout.precision(3);
		cout << student_grades[i].first << " " << student_grades[i].second << endl;
	}
	return 0;
}


#include<map>
#include<string>
#include<iostream>

using namespace std;

class Date{

public:

	Date(){};
	
	Date(int year,int month,int day);
	
	void NextDay(int n = 1);

	bool SetDate(int year, int month,int day);

	int year() const {return year_;}

	int month() const{return month_;}

	int day() const {return day_;}

private:

	static int GetDaysInYear(int year);

	static int ComputeDaysFromYearStart(int year, int month, int day);

	int day_,month_,year_;
};

struct InvalidDateException {
	string input_date;
	InvalidDateException(const std::string& str) : input_date(str) {}

};

std::ostream& operator<<(std::ostream& os, const Date& c);


std::istream& operator>>(std::istream& is, Date& c);

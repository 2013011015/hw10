#include <iostream>
#include <string>
#include "calendar.h"
using namespace std;

int main()
{
	Date date;
	string cmd;
	while (cmd!="quit")
	{
		try
		{
			cin >> cmd;
			if (cmd == "set")
			{
				cin >> date;
				cout << date << endl;
			}
			else if (cmd == "next"||cmd == "next_day")
			{
				if (cmd == "next")
				{
					int day;
					cin >> day;
					date.NextDay(day);
					cout<<date<<endl;
				}
				else
				{
					int day=1;
					date.NextDay(day);
					cout<<date<<endl;
				}
			}
		}
		catch (InvalidDateException& e)
		{
			cout<<"Invalid date: " <<e.input_date << endl;
		}
	}
	return 0;
}

